package main

//Escriba un programa que utilice métodos que reciban como parámetros dos cadenas de caracteres, y que muestre por pantalla un
//mensaje que indique si la primera de ellas es una subcadena de la segunda. Para dicho ejercicio, intente utilizar alguna función
//predefinida para dicho fin e intente implementar otra versión que NO utilice ninguna función predefinida. OBJETIVO: uso de librerías,
//métodos, paso de parámetros, retornos, ciclos.

import (
	"fmt"
	"strings"
)

func oraciones(ora string, orac string) bool {
	contiene := strings.Contains(ora, orac)
	return contiene
}
func main() {

	var ora string = "Mi casa es café con blanco"
	var orac string = "Mi casa es"
	var resultado bool = oraciones(ora, orac)
	fmt.Println("¿La oracion es subcadena de la primera?\n", resultado)
}
