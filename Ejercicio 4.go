package main

// Defina una estructura para un inventario de una tienda que vende zapatos. Cada zapato debe contar con la información de su modelo
// (marca), su precio y la talla del mismo que debe ir únicamente del 34 al 44.

import (
	"fmt"
	_ "fmt"
)

type zapateria struct {
	marca  string
	precio int
	talla  int
}

// Escriba un programa que lea la información anterior para 10 zapatos del inventario y los almacene en un arreglo.

var minimo int = 34
var maximo int = 44

func detalles(marca string, precio int, talla int) []zapateria {
	var zap []zapateria
	if talla < minimo || talla > maximo {
		fmt.Printf("La talla no esta disponible")
	} else {
		fmt.Println("Procesando.......")
		zapateriaa := zapateria{marca, precio, talla}
		zap = append(zap, zapateriaa)
	}
	return zap
}

//Escriba una función que reciba de parámetro dicho arreglo y dos tallas minimo y máximo, y que retorne un nuevo arreglo con los
//zapatos que concuerden con un el rango de tallas entre dichos mínimo y máximo. Documente la estrategia utilizada para crear
//un nuevo conjunto de elementos en tiempo de ejecución para ser retornado por la función.

func nuevasTallas(zap []zapateria, minimo1 int, maximo1 int) []zapateria {

	for i := 0; len(zap) > 0; i++ {
		if zap[i].talla >= minimo1 || zap[i].talla <= maximo1 {
			fmt.Println("Procesando.......")
			zap = append(zap, zap[i])
		}
	}
	return zap
}

//var arregloN [9]string
//var arregloP [9]string
//var arregloT [9]string

func main() {

	//zap []zapateria

	fmt.Println("Procesando clientes")

	detalles("eco", 17500, 34)
	detalles("shoe", 57700, 35)
	detalles("Tu zapato", 23050, 36)
	detalles("nike", 21600, 37)
	detalles("shoe", 7500, 38)
	detalles("nike", 17500, 39)
	detalles("eco", 57700, 40)
	detalles("Tu zapato", 23050, 41)
	detalles("shoe", 23800, 42)
	detalles("Tu zapato", 27500, 43)
	detalles("eco", 27500, 44)
	fmt.Println(nuevasTallas(zap, 34, 44))

}

//OBJETIVO: manejo de estructuras, ciclos, arreglos y slices (consideren el uso de slices para este ejercicio)
