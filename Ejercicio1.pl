%ROSE CAMPOS

%Defina sumlist(L, S) que es verdadero si S es la suma de los elementos de L.

sumlist([],0).
sumlist([L|Tail],S):-
laSuma(Tail,ss), s is ss+L.
