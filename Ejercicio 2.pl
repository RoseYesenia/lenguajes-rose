%ROSE CAMPOS

% Defina dos predicados largopar(L) y largoimpar(L) que son verdaderos
si los largos de la lista L son par o impar respectivamente.

largoDeDatos([],0).
largoDeDatos([_|Tail], L) :-

          largoDeDatos(Tail,X),
          L is X+1.

largopar(L):-

          largo(L,Y),
          0 is Y mod 2.

largoimpar(L):-

          largo(L,Y),
          1 is Y mod 2.
