package main

#include <stdio.h>
#include <malloc.h>
const int SIZE_ARRAY =10;

//punteros

void print_array(int *arreglo, int size){

    int* end_ptr = arreglo+size;
    while(arreglo< end_ptr){
    printf("%i ",*arreglo);
    arreglo++;

}

printf("\n");

}

void copy_array(int *source_ptr, int *dest_ptr, int size){
int* end_ptr = source_ptr+size;
while(source_ptr<end_ptr){
*dest_ptr = *source_ptr;
source_ptr++;
dest_ptr++;

}}

int insert_array(int* source_ptr, int ele, int size, int pos){

source_ptr = (int*)realloc(source_ptr,(size+1)*sizeof(int));//reasize a la location
//*(source_ptr+size) = ele;
int* limit_ptr = source_ptr+pos;
int* end_ptr = source_ptr+size;
// Se implement� ciclo para insertar en una posici�n espec�fica que contemple mover todos los elementos posteriores... SE INCLUY� EL PAR�METRO pos

while(end_ptr>limit_ptr){

*end_ptr = *(end_ptr-1);
end_ptr--;

}
*(source_ptr+pos)= ele;


// implementar ciclo para insertar en una posici�n espec�fica que contemple mover todos los elementos posteriores... DEBE INCLUIR EL PAR�METRO pos
return size+1;
}
int main() {

int arreglo[SIZE_ARRAY];
int *arreglo2 =  (int*) malloc(SIZE_ARRAY*sizeof(int));
arreglo[0] =9; arreglo[1]=8;arreglo[2] =7; arreglo[3] =6;arreglo[4] =5;
arreglo[5] =4;arreglo[6] =3;arreglo[7] =2;arreglo[8] =1;arreglo[9] =0;
printf("Primer arreglo sin puntero\n");
print_array(arreglo, SIZE_ARRAY);
printf("Copia del arreglo original a uno con puntero\n");
copy_array(arreglo, arreglo2, SIZE_ARRAY);
print_array(arreglo2, SIZE_ARRAY);
int new_size =0;

// package Proyectos

#include <stdio.h>
#include <malloc.h>
const int SIZE_ARRAY =10;
//punteros
void print_array(int *arreglo, int size){
int* end_ptr = arreglo+size;
while(arreglo< end_ptr){
printf("%i ",*arreglo);
arreglo++;
}
printf("\n");
}
void copy_array(int *source_ptr, int *dest_ptr, int size){
int* end_ptr = source_ptr+size;
while(source_ptr<end_ptr){
*dest_ptr = *source_ptr;
source_ptr++;
dest_ptr++;
}
}
int insert_array(int* source_ptr, int ele, int size, int pos){
source_ptr = (int*)realloc(source_ptr,(size+1)*sizeof(int));//reasize a la location
//*(source_ptr+size) = ele;
int* limit_ptr = source_ptr+pos;
int* end_ptr = source_ptr+size;
// Se implement� ciclo para insertar en una posici�n espec�fica que contemple mover todos los elementos posteriores... SE INCLUY� EL PAR�METRO pos
while(end_ptr>limit_ptr){
*end_ptr = *(end_ptr-1);
end_ptr--;
}
*(source_ptr+pos)= ele;


// implementar ciclo para insertar en una posici�n espec�fica que contemple mover todos los elementos posteriores... DEBE INCLUIR EL PAR�METRO pos
return size+1;
}
int main() {

int arreglo[SIZE_ARRAY];
int *arreglo2 =  (int*) malloc(SIZE_ARRAY*sizeof(int));
arreglo[0] =9; arreglo[1]=8;arreglo[2] =7; arreglo[3] =6;arreglo[4] =5;
arreglo[5] =4;arreglo[6] =3;arreglo[7] =2;arreglo[8] =1;arreglo[9] =0;
printf("Primer arreglo sin puntero\n");
print_array(arreglo, SIZE_ARRAY);
printf("Copia del arreglo original a uno con puntero\n");
copy_array(arreglo, arreglo2, SIZE_ARRAY);
print_array(arreglo2, SIZE_ARRAY);
int new_size =0;


//----------------------------------------------------------------------------------------------------------------------------------

//Termine el ejercicio de punteros en el lenguaje C desarrollado en clase de manera que permita insertar elementos al arrreglo de
//enteros usando punteros, pero en cualquier posici�n que se desee a partir de una posici�n brindada por par�metro.

new_size = insert_array(arreglo2,100,SIZE_ARRAY,6);
print_array(arreglo2, new_size);

new_size = insert_array(arreglo2,13,SIZE_ARRAY,9);
print_array(arreglo2, new_size);

new_size = insert_array(arreglo2,70,SIZE_ARRAY,14);
print_array(arreglo2, new_size);

new_size = insert_array(arreglo2,10,SIZE_ARRAY,13);
print_array(arreglo2, new_size);

new_size = insert_array(arreglo2,17,SIZE_ARRAY,8);
print_array(arreglo2, new_size);

return 0;

}
}
//----------------------------------------------------------------------------------------------------------------------------------
