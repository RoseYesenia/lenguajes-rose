%ROSE CAMPOS

% ------------------------------------------------------------------------

femenino(petra).
femenino(carmen).
femenino(maria).
femenino(rosa).
femenino(ana).
femenino(cande).

% ------------------------------------------------------------------------


madre(petra, juan).
madre(petra, rosa).
madre(carmen, pedro).
madre(maria, ana).
madre(maria, enrique).
madre(rosa, raul).
madre(rosa, alfonso).
madre(rosa, cande).

% ------------------------------------------------------------------------


masculino(angel).
masculino(pepe).
masculino(juan).
masculino(pedro).
masculino(enrique).
masculino(raul).
masculino(alfonso).

% ------------------------------------------------------------------------


padre(angel, juan).
padre(angel, rosa).
padre(pepe, pedro).
padre(juan, ana).
padre(juan, enrique).
padre(pedro, raul).
padre(pedro, alfonso).
padre(pedro, cande).

% ------------------------------------------------------------------------


progenitor(X, Y) :- padre(X, Y).
progenitor(X, Y) :- madre(X, Y).

% ------------------------------------------------------------------------


abuelo(X, Y) :- padre(X, Z), progenitor(Z, Y).
abuela(X, Y) :- madre(X, Z), progenitor(Z, Y).

% ------------------------------------------------------------------------

hermano(X,Y):- padre(Z,X),padre(Z,Y).
hermano(X,Y):- madre(Z,X),madre(Z,Y).

% ------------------------------------------------------------------------

%Deben existir sobrin@s para que haya una t�a

sobrino(X,Y):- hermano(Z,Y), padre(Z,X).
sobrino(X,Y):- hermano(Z,Y), madre(Z,X).

% ------------------------------------------------------------------------

%La relacion para ser t�a

tia(X,Y):-sobrino(Y,X).
parte(A,[A|_]).
parte(A,[_|Tail]) :- parte(A,Tail).

% -----------------------------------------------------------------------

eliminar_tias([],[]).
eliminar_tias([A|Tail],B):- parte(A,Tail),eliminar_tias(Tail,B).
eliminar_tias([A|Tail],[A|B]):- eliminar_tias(Tail,B).

% -----------------------------------------------------------------------

sin_Repetir(R,H,W,Z) :- findall([R,H],tia(R,H),W), eliminar_tias(W,Z).
%write('Se han eliminado las repeticiones').

% -----------------------------------------------------------------------
