%ROSE CAMPOS

% Defina la relaci�n subconj(S, S1), donde S y S1 son listas representando conjuntos, que es verdadera si S1 es subconjunto de S.

subconj([],_).
subconj([A|S1],[X|S]):- subconj(S1,S).
