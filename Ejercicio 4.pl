%ROSE CAMPOS

%Defina la relaci�n invertir(L, R) que es verdadera si R es la lista invertida de L.

invertir([],[]).
invertir([L|Y],R):- invertir(Y,X), append(X,[L],R).
