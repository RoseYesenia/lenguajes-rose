%ROSE CAMPOS
%
%A partir del siguiente conocimiento relacionado con la carta de un restaurante, escriba los hechos y las reglas necesarias para responder a las preguntas que se le hacen. Los elementos que interesan para el problema son los platos que se pueden consumir y una primera clasificaci�n puede ser la siguiente:

%--------------------------------------------------------------------

entrada(paella).
entrada(gazpacho).
entrada(consom�).

%--------------------------------------------------------------------


carne(filete_de_cerdo).
carne(pollo_asado).

%--------------------------------------------------------------------


pescado(trucha).
pescado(bacalao).

%--------------------------------------------------------------------


postre(flan).
postre(nueces_con_miel).
postre(naranja).

%--------------------------------------------------------------------

%1) Un plato principal es aquel que contiene o carne o pescado al menos
%
plato_Principal(A):- carne(A).
plato_Principal(A):- pescado(A).

%--------------------------------------------------------------------

%2) una comida completa se compone de una entrada, un plato principal y un postre.

comida_Completa(Entrada, Plato, Postre):- entrada(Entrada),plato_Principal(Plato), postre(Postre).

%--------------------------------------------------------------------

%Para completar la informaci�n que tenemos sobre las comidas del restaurante (reglas y hechos), se indica la lista de las calor�as que aporta cada plato:

calorias(paella, 200).
calorias(gazpacho,150).
calorias(consom�, 300).
calorias(filete_de_cerdo, 400).
calorias(pollo_asado, 280).
calorias(trucha, 160).
calorias(bacalao, 300).
calorias(flan, 200).
calorias(nueces_con_miel, 500).
calorias(naranja, 50).

%--------------------------------------------------------------------

%Siendo que el valor cal�rico de una comida lo determina la suma de las calor�as de cada uno de sus componentes,

valor_calorico(A,B,C,D):- calorias(A,J), calorias(B,K), calorias(C,Q), D is J+K+Q, comida_Completa(A,B,C).

%--------------------------------------------------------------------

%Eescriba los predicados adicionales para consultar todas las comidas que no superen un m�ximo X de calor�as indicadas en dicha consulta.

total_Maximo_De_Calorias(A,B,C,max) :- valor_calorico(A,B,C,D), max > D.

%write('El total de calorias es moderado').

%--------------------------------------------------------------------
