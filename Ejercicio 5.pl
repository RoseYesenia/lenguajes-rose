%ROSE CAMPOS

%Defina un predicado llamado partir(Lista, Umbral, Menores, Mayores) para dividir una lista respecto un umbral dado, dejando los valores menores a la izquierda y los mayores a la derecha. Por ejemplo, el resultado de partir la lista [2,7,4,8,9,1] respecto al umbral 6 ser�an las listas [2,4,1] y [7,8,9].

partir([],_,[],[]).
partir([L|M],L,[],M).
partir([L|M],X,[L|MM],I) :- X>=L,  partir(M,X,MM,I).

partir([L|M],L,[],M).
partir([L|M],X,MM,[L|MM],I) :- X<MM, partir(M,X,MM,I).
