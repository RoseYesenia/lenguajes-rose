package main

import "fmt"

// Escriba un programa que inserte un elemento en una determinada posición de un arreglo mediante el uso de
// punteros. El ejercicio debe contemplar la perdida mediante sustitución del elemento existente en dicho arreglo
// para la posición seleccionada. Una vez terminado el ejercicio, intente implementar otra variante del mismo que
// permita no perder el elemento sustituido, si no ampliar el tamaño original del arreglo. Mencione sus allazgos
// mediante documentación interna. OBJETIVO: Uso de arreglos y punteros e investigación para tratar de ampliar de
// forma dinámica el tamaño de un arreglo

func main() {

	//----------------------------------------------------------------------------------------------------------------------

	var arreglo [5]int
	arregloo := []string{"Hola Mundo"}

	arreglo[0] = 1
	arreglo[1] = 2
	arreglo[2] = 3
	arreglo[3] = 4
	arreglo[4] = 5

	lugar := arreglo[2]

	fmt.Println(arreglo[2])
	var puntero = &arreglo[lugar]
	*puntero = 6
	fmt.Println(arreglo)

	//----------------------------------------------------------------------------------------------------------------------

	arregloo = append(arregloo, "cruel")
	fmt.Println(arregloo)

}

//----------------------------------------------------------------------------------------------------------------------
