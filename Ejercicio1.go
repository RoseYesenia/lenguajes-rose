//ROSE YESENIA CAMPOS CARVAJAL

// Escriba un programa que a partir de una cadena (sea predefinida o leida del usuario), busque mediante
// iteracviones en dicha cadena, una palabra cualquiera. Debe indicar si se encuentra o no dicha palabra.
// OBJETIVO: Uso de variables tipo cadena de datos y ciclos.

package main

import (
	"fmt"
	"strings"
)

func main() {

	var frase string = "Mi zapato es verde"
	var palabra string = "zapato"
	var estado bool = false
	buscardor := strings.Fields(frase)

	for i := 0; len(frase) > 0; i++ {
		if palabra == buscardor[i] {
			estado = true
			fmt.Println(estado)
			return
		}
	}
	fmt.Println(estado)
	return
}
